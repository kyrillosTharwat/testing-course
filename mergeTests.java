import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
//Bitbuckit
public class mergeTests {

    @Test
    public void constructorTest(){
        int[] arr1 = { 1,2,7};
        int[] arr2 = { 0,5};
        int[] intArray = {0,1,2,5,7};

        Assertions.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr1,arr2),intArray);
    }
}
